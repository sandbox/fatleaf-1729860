# README.txt

**THIS MODULE IS DEPRECATED IN FAVOR OF imagecache_external
  (http://drupal.org/project/imagecache_external)**

Provides a simple mechanism for module and theme developers to use image
styles on image files not stored in the <code>files</code> directory. For
example, images supplied with a module.

The code in this module is based on a <a href="http://j.mp/PgqVSY">blog
post by seddonym</a>.

# Usage

Example: Using <code>styleexternal_image_style_external_url()</code> to get
the URL for a stylized version of an image included with a module:

<pre><code>
// Name of image style to use.
$img_style  = "thumbnail";

// Path to your desired image.
$img_path   = drupal_get_path('module', 'MYMODULE').'/img/example.png';

// Get the URL.
$url = styleexternal_image_style_external_url($img_style, $img_path);
</code></pre>

- - -

Example: Using <code>theme_image_style_external()</code> to setup a
renderable array for a stylized version of an image included with a module:

<pre><code>
// Name of image style to use.
$img_style  = "thumbnail";

// Path to your desired image.
$img_path   = drupal_get_path('module', 'MYMODULE').'/img/example.png';

// Create the renderable array with "image_style_external",
// provided by this module.
$render     = array(  '#theme' => 'image_style_external',
                      '#style_name' => $img_style,
                      '#path' => $img_path );

// This assigns a string of HTML to the variable $output.
$output = drupal_render($render);
</code></pre>
